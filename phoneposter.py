import os
import weechat


def checkResolution():
    resolution = str(os.popen("tmux display -p -t 0 '#{pane_width}x#{pane_height}'").read()) \
            .strip("\n")
    resList = ["54x29", "54x51"] #this is gross
    
    return resolution, resList

def infoState():
    weechat.command("", "/bar show nicklist")
    weechat.command("", "/bar show buflist")
    weechat.command("", "/bar set buflist position left")
    weechat.command("", "/bar set nicklist position left")

def normalState():
    weechat.command("", "/bar set buflist position left")
    weechat.command("", "/bar set nicklist position right")
    weechat.command("", "/bar hide nicklist")
    weechat.command("", "/bar hide buflist")








def checkInfo(data, buffer, args):
    global checkingState
    resolution, resList = checkResolution() 
    arg = args.split(" ",1)[0]

    if resolution == resList[0] or resolution == resList[1]: #Run command only in phone mode
        if arg == "":
            if checkingState == 0:
                checkingState = 1
                infoState()
            elif checkingState == 1:
                checkingState = 0
                normalState()

        elif arg == "check":
            checkingState = 1
            infoState()
    
        elif arg == "checked" or arg == "done":
            checkingState = 0
            normalState()
        
        else:
            weechat.prnt("", "invalid command: try [check] or [done]")
            checkingState = 2
    else:
        weechat.prnt("", "error: Not in phone mode")
        checkingState = 2
    
    return weechat.WEECHAT_RC_OK









def changeLayout(data, remaining_calls):
    global checkingState
    resolution, resList = checkResolution()

    if resolution == resList[0] or resolution == resList[1]:

        if checkingState != 1: #Make sure buffers aren't closed when checking info
            weechat.command("", "/bar hide nicklist")
            weechat.command("", "/bar hide buflist")
            weechat.command("", "/mute /set weechat.look.buffer_time_format %H:%M")
            weechat.command("", "/mute /set weechat.look.prefix_align_max 7")
        
        elif checkingState == 1:
            pass

    else:
        weechat.command("", "/bar show nicklist")
        weechat.command("", "/bar show buflist")
        weechat.command("", "/mute /set weechat.look.buffer_time_format %H:%M:%S")
        weechat.command("", "/mute /set weechat.look.prefix_align_max 0")
    
    return weechat.WEECHAT_RC_OK









def main():
    #init plugin
    NAME = "dynamic_size"
    AUTHOR = "wnabee"
    VERSION = "1.0"
    LICENCE = "MIT"
    DESCRIPTION = "change the layout at certain resolutions"
    weechat.register(NAME,AUTHOR,VERSION,LICENCE,DESCRIPTION, "","")
    
    #init script
    global checkingState
    checkingState = 0
    
    #Layout change check
    weechat.hook_timer(200,0,0,"changeLayout","")
    
    #Information check command
    weechat.hook_command("i",
                         "check the buffer and nick list",
                         "[check] | [checked|done]",
                         "check info | stop checking info",
                         "check || checked || done",
                         "checkInfo",
                         "")

if __name__=="__main__":
    main()
